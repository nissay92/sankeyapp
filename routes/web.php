<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (auth()->check()) {
        return redirect('/home');
    }
    return redirect('login');
});


Auth::routes();

Route::get('/home', 'FileController@showUploadForm')->name('upload')->middleware('auth');


Route::get('upload', 'FileController@showUploadForm')->name('upload')->middleware('auth');
Route::post('upload', 'FileController@storeFile');

Route::get('uploadedFile/{filename}', 'FileController@getFile')->name('get.file')->middleware('auth');
Route::get('uploaded/{filename}', 'SankeyController@getFile')->name('getfile')->middleware('auth');
Route::get('/sankey', 'SankeyController@index')->name('sankey')->middleware('auth');
Route::delete('/file/{name}','SankeyController@destroy')->name('deletefile');