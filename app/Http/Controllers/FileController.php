<?php

namespace App\Http\Controllers;

use App\File;

use Illuminate\Http\Request;

class FileController extends Controller
{
    //
    public function showUploadForm(){
        return view ('upload');
    }

    public function storeFile(request $request)
    {
        //
        if ($request->hasFile('file')) {
            $files = $request->file('file');


            $this->validate($request,[
               'file.*'=>'required|mimes:csv,txt'
            ]);
            foreach ($files as $file){

            $filename = $file->getClientOriginalName();
            $filesize = $file->getClientSize();

            $filepath = base_path() . '/public/files';

            if (!file_exists(base_path() . '/public/files/'.$filename)){
            $file->move($filepath,$filename);

            $filemodel = new File;
            $filemodel->name = $filename;
            $filemodel->path = $filepath;
            $filemodel->size = $filesize;
            $filemodel->save();
            }else{
                return back()->with('warning','The name of the file already exists');

            }


            }
            //
            return back()->with('success','All Files are uploaded Successfully');

        } else {
            return back()->with('warning','No file was selected');
        }


    }



    public function getFile($filename){

            try{
                return response()->download(Storage_path('app/public/'.$filename),null,[],null);
            }catch(\Exception $e){
                return 'File Not Found';
            }

    }

}
