<?php

namespace App\Http\Controllers;
use App\File;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class SankeyController extends Controller
{
    //
    public function index(){
        $files = File::all();
        return view ('sankey',['files'=>$files]);
    }

    public function getFile($filename)
    {

        try {
            return response()->download(Storage_path('app/public/'
                . $filename), null, [], null);
        } catch (\Exception $e) {
            return 'File Not Found';
        }
    }

    public function destroy($name){
        $del = File::where('name',$name)->first();
        unlink(public_path('files/'.$name));
        $del->delete();
        return redirect('/sankey');
    }
}
