@extends('layouts.app')

@section('content')

    <style>

        .node rect {
            cursor: move;
            fill-opacity: .9;
            shape-rendering: crispEdges;
        }

        .node text {
            pointer-events: none;
            text-shadow: 0 1px 0 #fff;
        }

        .link {
            fill: none;
            stroke: #000;
            stroke-opacity: .2;
        }

        .link:hover {
            stroke-opacity: .5;
        }

    </style>
    <script type="text/javascript">
        <!--
        function toggle_visibility(id) {
            var e = document.getElementById(id);
            if(e.style.display == 'none'){
                $(e).toggle(750);

            }
            else
                $(e).toggle(750);
        }
        //-->
    </script>



    <script src="{{ asset('js/d3.js') }}"></script>
    <script src="{{ asset('js/sankey.js') }}"></script>
    <script>
        var units = "Euros";

        var margin = {top: 10, right: 10, bottom: 10, left: 10},
            width = 900 - margin.left - margin.right,
            height = 500 - margin.top - margin.bottom;

        var formatNumber = d3.format(",.0f"),    // zero decimal places
            format = function (d) {
                return formatNumber(d) + " " + units;
            },
            color = d3.scale.category20();

        var instances = [];

        function init_sankey(graph, svg, width, height)
        {
            // Set the sankey diagram properties
            var sankey = d3.sankey()
                .nodeWidth(36)
                .nodePadding(40)
                .size([width, height]);

            var path = sankey.link();

            var updated = sankey
                .nodes(graph.nodes)
                .links(graph.links)
                .layout(32);

            // add in the links
            var link = svg.append("g").selectAll(".link")
                .data(graph.links)
                .enter().append("path")
                .attr("class", "link")
                .attr("d", path)
                .style("stroke-width", function (d) {
                    return Math.max(1, d.dy);
                })
                .sort(function (a, b) {
                    return b.dy - a.dy;
                });

            // add the link titles
            link.append("title")
                .text(function (d) {
                    return d.source.name + " → " +
                        d.target.name + "\n" + format(d.value);
                });

            // add in the nodes
            var node = svg.append("g").selectAll(".node")
                .data(graph.nodes)
                .enter().append("g")
                .attr("class", "node")
                .attr("transform", function (d) {
                    return "translate(" + d.x + "," + d.y + ")";
                })
                .call(d3.behavior.drag()
                    .origin(function (d) {
                        return d;
                    })
                    .on("dragstart", function () {
                        this.parentNode.appendChild(this);
                    })
                    .on("drag", dragmove));

            // add the rectangles for the nodes

            node.append("rect")
                .attr("height", function (d) {
                    return d.dy;
                })
                .attr("width", sankey.nodeWidth())
                .style("fill", function (d) {
                    return d.color = color(d.name.replace(/ .*/, ""));
                })
                .style("stroke", function (d) {
                    return d3.rgb(d.color);
                })
                .append("title")
                .text(function (d) {
                    return d.name + "\n" + format(d.value);
                });

            // add in the title for the nodes
            node.append("text")
                .attr("x", -6)
                .attr("y", function (d) {
                    return d.dy / 2;
                })
                .attr("dy", ".35em")
                .attr("text-anchor", "end")
                .attr("transform", null)
                .text(function (d) {
                    return d.name;
                })
                .filter(function (d) {
                    return d.x < width / 2;
                })
                .attr("x", 6 + sankey.nodeWidth())
                .attr("text-anchor", "start");

            // the function for moving the nodes
            function dragmove(d) {
                d3.select(this).attr("transform",
                    "translate(" + d.x + "," + (
                        d.y = Math.max(0, Math.min(height - d.dy, d3.event.y))
                    ) + ")");
                sankey.relayout();
                link.attr("d", path);
            }
        }

        // append the svg canvas to the page
        function graphsvg(link,id) {

            width = $('.snk').width() - 60;
             //height = $('.snk').height();

            var svg = d3.select(id).append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.top + margin.bottom)
                .attr("xmlns", "http://www.w3.org/2000/svg")
                .attr("version", "1.1")
                .attr("class","svgg")
                .append("g")
                .attr("transform",
                    "translate(" + margin.left + "," + margin.top + ")");



            // load the data (using the timelyportfolio csv method)
            d3.csv(link, function (error, data) {

                //set up graph in same style as original example but empty
                var graph = {"nodes": [], "links": []};

                data = data.filter(function (item)
                {
                    return item.source !== item.target;
                });

                data.forEach(function (d) {
                    graph.nodes.push({"name": d.source});
                    graph.nodes.push({"name": d.target});
                    graph.links.push({
                        "source": d.source,
                        "target": d.target,
                        "value": +d.value
                    });
                });

                // return only the distinct / unique nodes
                graph.nodes = d3.keys(d3.nest()
                    .key(function (d) {
                        return d.name;
                    })
                    .map(graph.nodes));

                // loop through each link replacing the text with its index from node
                graph.links.forEach(function (d, i) {
                    graph.links[i].source = graph.nodes.indexOf(graph.links[i].source);
                    graph.links[i].target = graph.nodes.indexOf(graph.links[i].target);
                });

                //now loop through each nodes to make nodes an array of objects
                // rather than an array of strings
                graph.nodes.forEach(function (d, i) {
                    graph.nodes[i] = {"name": d};
                });

                instances.push({
                    id: id,
                    graph: graph,
                    svg: svg
                });

                init_sankey(graph, svg, width, height);
            });

            function resize() {
                var instance;

                for (var i = 0; i < instances.length; i++)
                {
                    instance = instances[i];

                    width = $('.snk').width() - 60;

                    instance.svg.selectAll(".link").remove();
                    instance.svg.selectAll(".node").remove();

                    // $(instance.id + ' .svgg').attr('width', width);

                    init_sankey(instance.graph, instance.svg, width, height);
                }
            };

            d3.select(window).on('resize', resize);
        }
    </script>

    @if (count($files)>0)
        @foreach($files as $file)
            <div class="container pad">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="row fname">

                            <div class="col-lg-6" >
                                <p class="snktitle" onclick="toggle_visibility('chart{{$file->id}}');"><i class="fas fa-file-invoice"></i> {{$file->name}}</p>
                            </div>

                            <div class="col-lg-3">
                                <p class="snktitle">{{round(($file->size)/1024,2)}}KB</p>
                            </div>
                            <div class="col-lg-3 right">
                                <form action="{{route('deletefile',$file->name)}}" method="post" class="delform">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger rad">X</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 snk">
                        <div class="card graphpic">
                            <div class="card-body ">

                                <p id="chart{{$file->id}}" style="display: none;">

                                    <script>
                                        graphsvg("{{asset('files/'.$file->name)}}","#chart{{$file->id}}");
                                    </script>
                                </p>


                            </div>
                        </div>

                    </div>

                </div>
            </div>

        @endforeach
    @else
        <div class="full-height flex-center">
            <div class="container">
                <h1 class="snktitle center">No Files uploaded</h1>
            </div>
        </div>
    @endif
@endsection
