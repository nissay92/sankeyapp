@extends('layouts.app')

@section('content')
 <div   class="full-height flex-center">
<div class="container">
    <div class="row justify-content-center">
    <div class="col-lg-4">
    <div class="card logincard">


        <div class="card-body">

            <form method="POST" action="/upload" enctype="multipart/form-data" autocomplete="off">

                @csrf

                <div class="form-group">

                    <img src="/img/bgupload.png" class="imageup">

                    <label><input type="file" name="file[]" class="form-control dropzone" id="dropzone" multiple/></label>
                    <br>

                </div>
                <div class="form-group">
                <div id="selectedFiles">

                </div>
                </div>

                <div class="form-group center">
                <label class="labelfile">Click on the image to upload OR drag and drop your files</label>

                </div>
                <div class="form-group">
                    <input type="submit" value="Upload" class="btn btn-primary uploadbtn">
                </div>

            </form>
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    Upload Validation Error<br>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>

            @endif
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <strong>{{ $message }}</strong>
                    <a class="btn btn-primary" href="{{ route('sankey') }}">Visualize Data</a>
                </div>
            @endif
            @if ($message = Session::get('warning'))
                <div class="alert alert-danger alert-block">
                    <strong>{{ $message }}</strong>
                </div>
            @endif

        </div>

    </div>
    </div>
    </div>
</div>
 </div>
 s
@endsection